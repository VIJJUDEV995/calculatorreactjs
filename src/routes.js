import React from 'react';
import {Router, Route, Redirect} from 'react-router';

import App from './App'
import Login from './Components/login';
import Signup from './Components/signup';
import Calculator from './Components/calculator';

const Routes = (props) => (
    <Router {...props}>
        <Redirect from="/" to="/Login"/>
        <Route path="/Login" component={Login}/>
        <Route path="/Dashboard" component={App}/>
        <Route path="/Signup" component={Signup}/>
        <Route path="/Calculator" component={Calculator}/>
    </Router>
);

export default Routes;