import './index.css';
import React from 'react';
import Routes from './routes';
import ReactDOM from 'react-dom';
import { browserHistory } from 'react-router';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
    <Routes history={browserHistory} />,
    document.getElementById('root')
);
registerServiceWorker();
