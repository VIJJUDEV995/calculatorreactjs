import React, {Component} from 'react';
import {browserHistory} from 'react-router';
import swal from 'sweetalert';
import './login.css';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: '',
            passWord: '',
            isValidated: false
        }
    }
    usernameChange(e) {
        this.setState({userName: e.target.value})
    }
    passChange(e) {
        this.setState({passWord: e.target.value})
    }
    validateForm() {
        this.state.isValidated = false;
        let name = this.state.userName,
            pass = this.state.passWord;

        if (name === "") {
            swal({title: "Please Enter Email", icon: "warning"});
        } else if (pass === "") {
            swal({title: "Please Enter Password", icon: "warning"});
        } else {
            this.state.isValidated = true;
        }
    }

    loginClick() {
        this.validateForm();
        if (this.state.isValidated) {
            var data = new FormData();
            var data1 = {};
            data1.UserName = this.state.userName;
            data1.Password = this.state.passWord;
            data1.Success = false;
            data.append("data", JSON.stringify(data1));
            fetch('http://35.184.44.69/logincontroller.ashx', {
                    method: "POST",
                    body: data
                }).then(function (response) {
                return response.json();
            })
                .then(function (data) {
                    if (data.Success) {
                        browserHistory.push('/Dashboard');
                    } else {
                        swal({title: data.Message, text: "Please Enter Correct Credentials!", icon: "error", dangerMode: true})
                    }
                });
        }
    }

    signupClick() {
        browserHistory.push('/Signup');
    }

    render() {
        return (
            <div className="login-page ng-scope ui-view">
                <div className="row">
                    <div className="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                        <img src={require(".././images/lock.png")} className="user-avatar" alt="Lock"/>
                        <h1>Login
                            <small>Enter Your Credentials</small>
                        </h1>
                        <form role="form" className="ng-pristine ng-valid">
                            <div className="form-content">
                                <div className="form-group">
                                    <input
                                        type="text"
                                        className="form-control input-underline input-lg placeholderText"
                                        placeholder="Email *"
                                        onChange={this
                                        .usernameChange
                                        .bind(this)}/>
                                </div>
                                <div className="form-group">
                                    <input
                                        type="password"
                                        className="form-control input-underline input-lg placeholderText"
                                        placeholder="Password *"
                                        onChange={this
                                        .passChange
                                        .bind(this)}/>
                                </div>
                            </div>
                            <button
                                type="button"
                                className="btn btn-white btn-outline btn-lg btn-rounded"
                                onClick={this
                                .loginClick
                                .bind(this)}>Login</button>
                        </form>
                        <div>
                            <p className="signupText">don't have account?
                                <small
                                    className="crsrpntr"
                                    onClick={this
                                    .signupClick
                                    .bind(this)}>SignUp Here</small>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;
