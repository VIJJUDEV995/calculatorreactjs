import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import swal from 'sweetalert';
import './login.css';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: '',
            passWord: '',
            email: '',
            pass2: '',
            isValidated: false
        }
    }
    emailChange(e) {
        this.setState({ email: e.target.value })
    }
    passChange(e) {
        this.setState({ passWord: e.target.value })
    }
    usernameChange(e) {
        this.setState({ userName: e.target.value });
    }
    pass2Change(e) {
        this.setState({ pass2: e.target.value });
    }
    loginClick() {
        browserHistory.push('/Login');
    }

    validateForm() {
        this.state.isValidated = false;
        let name = this.state.userName,
            email = this.state.email,
            pass1 = this.state.passWord,
            pass2 = this.state.pass2;

        if (name === "") {
            swal({
                title: "Please Enter Your Name",
                icon: "warning"
            });
        } else if (email === "") {
            swal({
                title: "Please Enter Your Email Address!",
                icon: "warning"
            });
        } else if (pass1 === "") {
            swal({
                title: "Please Enter a Password",
                icon: "warning"
            });
        }
        else if (pass2 === "") {
            swal({
                title: "Please Enter Confirming Password",
                icon: "warning"
            });
        } else if (pass1 != pass2) {
            swal({
                title: "Passwords don't Match",
                text: "Please Enter Correct Passwords",
                icon: "warning"
            });
        } else {
            this.state.isValidated = true;
        }
    }

    signupClick() {
        this.validateForm();
        if (this.state.isValidated) {
            // var data = new FormData();
            // var data1 = {};
            // data1.UserName = this.state.userName;
            // data1.Password = this.state.passWord;
            // data1.Success = false;
            // data.append("data", JSON.stringify(data1));
            // fetch('http://35.184.44.69/logincontroller.ashx', {
            //     method: "POST",
            //     body: data
            // })
            //     .then(function (response) {
            //         return response.json();
            //     }).then(function (data) {
            //         if (data.Success) {
            //             browserHistory.push('/Dashboard');
            //         } else {
            //             alert(data.Message);
            //         }
            //     });
            browserHistory.push('/Dashboard');
        }
    }

    render() {
        return (
            <div className="login-page ng-scope ui-view">
                <div className="row">
                    <div className="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                        <img src={require(".././images/signup.png")} className="user-avatar" alt="Lock" />
                        <h1>Signup Here <small>Enter Your Details</small></h1>
                        <form role="form" className="ng-pristine ng-valid">
                            <div className="form-content">
                                <div className="form-group">
                                    <input type="text" className="form-control input-underline input-lg placeholderText" placeholder="Name / User Name *"
                                        onChange={this.usernameChange.bind(this)} />
                                </div>
                                <div className="form-group">
                                    <input type="text" className="form-control input-underline input-lg placeholderText" placeholder="Email *"
                                        onChange={this.emailChange.bind(this)} />
                                </div>
                                <div className="form-group">
                                    <input type="password" className="form-control input-underline input-lg placeholderText" placeholder="Password *"
                                        onChange={this.passChange.bind(this)} />
                                </div>
                                <div className="form-group">
                                    <input type="password" className="form-control input-underline input-lg placeholderText" placeholder="Confirm Password *"
                                        onChange={this.pass2Change.bind(this)} />
                                </div>
                            </div>
                            <button type="button" className="btn btn-white btn-outline btn-lg btn-rounded" onClick={this.signupClick.bind(this)}>Sign Up</button>
                        </form>
                        <div><p className="signupText">already have an account? <small className="crsrpntr" onClick={this.loginClick.bind(this)}>Login Here</small></p></div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;
