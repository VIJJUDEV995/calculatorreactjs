import React, {Component} from 'react';
import {browserHistory} from 'react-router';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  calcClick()
  {
    browserHistory.push('/Calculator');
  }

  render() {
    return (
      <div className="login-page ng-scope ui-view">
        <p className="fs45">Dashboard is under Development Thanks for Registration!</p>
        <br/>
        <h2 className="signupText">Click Here to Go to Calculator App
          <small
            className="crsrpntr"
            onClick={this
            .calcClick
            .bind(this)}>Calculator React JS App</small>
        </h2>
      </div>
    );
  }
}

export default App;
